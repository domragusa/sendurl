#!/usr/bin/env python3

import json
import sys
from pydbus import SessionBus
from gi.repository import GLib


class json_over_stdio:
    """Class implementing the serialization mechanism used by WebExtension
    Native Messaging: an object is converted to and from an utf-8 encoded
    JSON string appended to the 32bit integer containing its size.
    
    References:
    https://developer.chrome.com/extensions/messaging
    https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Native_messaging
    """
    def __init__(self, buf_in=sys.stdin.buffer, buf_out=sys.stdout.buffer):
        """Constructor, takes two optional arguments:
            - buf_in, the buffer used to read from (defaults to stdin)
            - buf_out, the buffer used to write to (defaults to stdout)
        """
        self.buf_in  = buf_in
        self.buf_out = buf_out
    
    def get(self):
        """Read an object from buf_in and return it unserialized as a dict."""
        raw_len = self.buf_in.read(4)
        if not raw_len: raise Exception
        
        int_len = int.from_bytes(raw_len, byteorder=sys.byteorder)
        data = self.buf_in.read(int_len).decode('utf-8')
        return json.loads(data)
    
    def put(self, data):
        """Serialize and write to buf_out the dict passed as argument data."""
        enc_data = json.dumps(data).encode('utf-8')
        enc_len  = len(enc_data).to_bytes(4, byteorder=sys.byteorder)
        
        self.buf_out.write(enc_len + enc_data)
        self.buf_out.flush()

class stdio_server:
    """Class implementing the server logic for a Native Messaging app following
    a simple protocol.
    Each message is a json object with proprieties:
        - type, used to identify the kind of request;
        - data, request's parameters.
    
    Registration of a type of request is done by adding a handler function to
    stdio_server.handlers[request_type_name].
    
    A handler function takes two arguments:
        - iface, a state object memorized by the server.
        - data, parameters for the current request;
    and returns a tuple:
        - error_code, negative on error otherwise 0;
        - response_data, data to transmit back (None to avoid response).
    """
    def __init__(self, iface):
        """Constructor."""
        self.iface = iface
        self.tunnel = json_over_stdio()
        self.handlers = {}
        
        ch = GLib.IOChannel.unix_new(sys.stdin.fileno())
        GLib.io_add_watch(ch, GLib.IO_IN, lambda *_: self.process_one() or True)
    
    def run(self):
        """Start GLib MainLoop."""
        loop = GLib.MainLoop()
        loop.run()
    
    def process_one(self):
        """Read a request from stdin and process it.
        (Shouldn't be called directly)"""
        try:
            req = self.tunnel.get()
        except:
            return
        
        if not self._is_valid_request(req): return
        if not req['type'] in self.handlers: return
        
        kind = req['type']
        data = req.get('data', None)
        
        return self._answer_request(kind, data)
    
    def _answer_request(self, kind, data=None):
        """Answer a single request.
        Shouldn't be used directly, but can be useful when dealing with events,
        ex. simulating a request as a response to a signal."""
        errno, info = self.handlers[kind](self.iface, data)
        
        if errno < 0:
            self._send_raw_response('error', {'errno': -errno, 'info': info})
        else:
            self._send_raw_response('answer_to_'+kind, info)
    
    def _send_raw_response(self, kind, data):
        self.tunnel.put({'type': kind, 'data': data})
    
    @staticmethod
    def _is_valid_request(req=None):
        return isinstance(req, dict) and 'type' in req

class kdeconnect_iface:
    """Class to interface with KDEConnect via DBus, exposing device
    enumeration and url sharing functionality.
    
    kdeconnect_iface.callback_list_changed is the function to call
    to notify device list updates, just set it directly, no arguments
    will be passed."""
    def __init__(self):
        """Constructor."""
        self.bus = SessionBus()
        self.callback_list_changed = None
        
        obj = self._get_dbus_obj()
        obj.deviceVisibilityChanged.connect(self._callback)
    
    def _callback(self, *_):
        if self.callback_list_changed is not None:
            self.callback_list_changed()
    
    def _get_dbus_obj(self, rel_path=''):
        return self.bus.get(
            'org.kde.kdeconnect', '/modules/kdeconnect'+rel_path)
        
    def get_list(self):
        """Get the list of registered and available devices.
            Return values:
                - None, on error;
                - {} (empty dict), when no device is available;
                - dict addressed by device id, containing name and type
                    information.
        """
        try:
            obj = self._get_dbus_obj()
            id_list = obj.devices()
            device_list = {}
            for cur_id in id_list:
                device = self._get_dbus_obj('/devices/'+cur_id)
                
                # skip unreachable and untrusted devices
                if not device.isReachable or not device.isTrusted: continue
                
                device_list[cur_id] = {'name': device.name, 'type': device.type}
            return device_list
        except:
            return None
    
    def send_url(self, dev_id, url):
        """Send url to the device corresponding to dev_id.
            Return values:
                - True, when everything went ok.
                - False, otherwise."""
        try:
            obj = self._get_dbus_obj(f'/devices/{dev_id}/share')
            obj.shareUrl(url)
            return True
        except:
            return False

def get_server_instance():
    """Get an initialized instance of the server ready to be run."""
    def get_list_handler(iface, _):
        dev_list = iface.get_list()
        return (-5, 'couldn\'t perform action') if dev_list is None else (0, dev_list)
    
    def send_url_handler(iface, data):
        try:
            dev = data['dev']
            url = data['url']
        except:
            return (-2, 'malformed request')
        res = iface.send_url(dev, url)
        return (-3, 'couldn\'t perform action') if not res else (0, None)
    
    iface = kdeconnect_iface()
    serv = stdio_server(iface)
    
    serv.handlers['get_list'] = get_list_handler
    serv.handlers['send_url'] = send_url_handler
    cb_get_list = lambda: serv._answer_request('get_list')
    iface.callback_list_changed = cb_get_list
    
    return serv

if __name__ == '__main__':
    serv = get_server_instance()
    serv.run()

