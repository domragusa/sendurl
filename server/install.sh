#!/bin/sh -e

PATH_MANIFEST="$HOME/.mozilla/native-messaging-hosts/sendurl_manifest.json"
PATH_SERVER="$HOME/.local/bin"

echo "Installation of SendURL server."

echo "Installation directory for server [default: ~/.local/bin]":
read PATH_TMP
if [ -n "$PATH_TMP" ]; then
    PATH_SERVER="$PATH_TMP"
fi

PATH_SERVER="$PATH_SERVER/sendurl_server.py"

cd "$(dirname "$0")"

mkdir -p "$(dirname "$PATH_SERVER")"
cp "./server.py" "$PATH_SERVER"

mkdir -p "$(dirname "$PATH_MANIFEST")"
sed "s|PATH_TO_SERVER|$PATH_SERVER|" "./sendurl_manifest.json" > "$PATH_MANIFEST"

echo "Installation successful!"
