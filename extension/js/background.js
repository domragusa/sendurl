let extserv = new ExternalConnection('sendurl_server', {deviceList: {}});
let intserv = new InternalConnection();

extserv.handlers['answer_to_get_list'] =
    (session, data) => {
        session.deviceList = data;
        intserv.transmit('list_data', data);
    };
extserv.transmit('get_list');

intserv.handlers['ask_list'] =
    () => {
        intserv.transmit('list_data', extserv.session.deviceList);
    };
intserv.handlers['send_url'] =
    (session, data) => {
        extserv.transmit('send_url', data);
    };
