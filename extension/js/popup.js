function getDiff(oldObj, newObj){
    let filter_keys = (obj, fn) => Object.keys(obj).filter(fn),
        obj_equal   = (obj1, obj2) => obj1.name == obj2.name && obj1.type == obj2.type;
    
    return {
        added:    filter_keys(newObj, (x)=> !(x in oldObj)),
        removed:  filter_keys(oldObj, (x)=> !(x in newObj)),
        modified: filter_keys(oldObj, (x)=> (x in newObj) && !obj_equal(oldObj[x], newObj[x]))
    };
}

class PopupUI{
    constructor(callback){
        this.container = document.querySelector('#list');
        this.devices = {};
        this.callback = ()=>{};
    }
    
    addDevice(id, name, type){
        let li = document.createElement('li');
        
        this.devices[id] = {elem: li};
        li.addEventListener('click', () => {
            if(!li.classList.contains('disabled'))
                this.sendTo(id)}
        );
        
        this.updateDevice(id, name, type);
        this.container.append(li);
    }
    
    disableDevice(id){
        let cur = this.devices[id];
        if(!cur) return;
        
        cur.elem.classList.add('disabled');
        cur.name = '';
        cur.type = '';
    }
    
    updateDevice(id, name, type){
        let cur = this.devices[id];
        if(!cur) return;
        
        cur.elem.textContent = name +' ('+ type +')';
        cur.elem.className = 'dev-'+type;
        cur.name = name;
        cur.type = type;
    }
    
    updateList(devlist){
        let diff = getDiff(this.devices, devlist);

        for(let id of diff.removed){
            this.disableDevice(id);
        }
        
        for(let id of diff.added){
            let cur = devlist[id];
            this.addDevice(id, cur.name, cur.type);
        }
        
        for(let id of diff.modified){
            let cur = devlist[id];
            this.updateDevice(id, cur.name, cur.type);
        }
    }
    
    sendTo(device){
        let tmp = browser.tabs.query(
            {active: true, currentWindow: true}).then(tab => tab[0].url);
        tmp.then(url => this.callback({dev: device, url: url}));
    }
}

window.addEventListener('DOMContentLoaded', () => {
    let ui = new PopupUI();
    
    let connection = new InternalConnection();
    ui.callback = (data) => connection.transmit('send_url', data);
    connection.handlers['list_data'] = (session, data) => ui.updateList(data);
    connection.transmit('ask_list');
});
