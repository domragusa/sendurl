class BaseConnection{
    constructor(port, session){
        this.port    = port;
        this.session = session;
        this.handlers = {};
        
        port.onMessage.addListener((msg)=>this.receive(msg));
    }
    
    receive(msg){
        let type = msg.type,
            data = msg.data;
        
        if(!(type in this.handlers)) return;
        let res = this.handlers[type](this.session, data);
        
        if(res){
            return Promise.resolve(res);
        }
    }
    
    transmit(type, data){
        let msg = {type: type, data: data};
        this.rawTransmit(msg);
    }
}

class InternalConnection extends BaseConnection{
    constructor(session={}){
        super(browser.runtime, session);
    }
    
    rawTransmit(data){
        this.port.sendMessage(data);
    }
}

class ExternalConnection extends BaseConnection{
    constructor(name, session={}){
        let port = browser.runtime.connectNative(name);
        super(port, session);
    }
    
    rawTransmit(data){
        this.port.postMessage(data);
    }
}
