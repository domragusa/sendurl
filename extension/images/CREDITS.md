Credits
=======

- dev-desktop.svg
    * [round-computer-24px.svg @ material.io](https://material.io/tools/icons/static/icons/round-computer-24px.svg)
- dev-smartphone.svg
    * [round-smartphone-24px.svg @ material.io](https://material.io/tools/icons/static/icons/round-smartphone-24px.svg)
- dev-tablet.svg
    * [round-tablet-24px.svg @ material.io](https://material.io/tools/icons/static/icons/round-tablet-24px.svg)
- dev-unknown.svg
    * [round-device_unknown-24px.svg @ material.io](https://material.io/tools/icons/static/icons/round-device_unknown-24px.svg)
- icon.svg
    * [round-devices-24px.svg @ material.io](https://material.io/tools/icons/static/icons/round-devices-24px.svg)
